import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() {
  }

  getUsers() {
    return [
      {
        name: 'moataz',
        age: 29,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, aut blanditiis '
      },
      {
        name: 'mohamed',
        age: 34,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, aut blanditiis '
      },
      {
        name: 'ali',
        age: 34,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, aut blanditiis '
      },
      {
        name: 'islam',
        age: 34,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, aut blanditiis '
      },
      {
        name: 'omar',
        age: 34,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, aut blanditiis '
      },

    ];

  }
}
