import {Component, OnInit} from '@angular/core';
import {UsersService} from '../users.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.sass']
})
export class UserListComponent implements OnInit {
  private mutesd: boolean;
  private users: { name: string; age: number; description: string }[];

  constructor(usersService: UsersService) {
    this.mutesd = true;
    this.users = usersService.getUsers();
  }

  ngOnInit() {

  }

}
