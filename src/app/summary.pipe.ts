import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'summary'
})
export class SummaryPipe implements PipeTransform {

  transform(text: string, num: number = 10): string {

    return text.substr(0, num);
  }

}
