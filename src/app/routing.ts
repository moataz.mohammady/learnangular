import {Routes} from '@angular/router';

import {CalculatorComponent} from './calculator/calculator.component';
import {UserListComponent} from './user-list/user-list.component';

export const appRoutes: Routes = [
  {path: '', component: UserListComponent},
  {path: 'calculator', component: CalculatorComponent},
  {path: 'users', component: UserListComponent}
];
