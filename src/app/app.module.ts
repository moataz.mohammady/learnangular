import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UserListComponent} from './user-list/user-list.component';
import {CalculatorComponent} from './calculator/calculator.component';
import {SummaryPipe} from './summary.pipe';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routing';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    CalculatorComponent,
    SummaryPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
